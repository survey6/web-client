import Vue from 'vue'
import VueApollo from 'vue-apollo'
import { ApolloClient } from 'apollo-client'
import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { split, ApolloLink, from } from 'apollo-link'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'
import { onError } from 'apollo-link-error'

export default function ({ store, app }) {
  const connectionParams = () => {
    return {
      headers: {
        authorization: store.getters['user/isLogged_in']
          ? `Bearer ${store.getters['user/getToken']}`
          : undefined,
      },
    }
  }

  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (
      graphQLErrors &&
      graphQLErrors[0].extensions &&
      (graphQLErrors[0].extensions.code === 'invalid-jwt' ||
        graphQLErrors[0].extensions.code === 'start-failed')
    ) {
      store.dispatch('user/removeStateA')
      sessionStorage.removeItem('user')
      console.log('Session expired! Please login')
      app.router.replace('/')
      return
    }

    if (
      networkError &&
      networkError.extensions &&
      (networkError.extensions.code === 'invalid-jwt' ||
        networkError.extensions.code === 'start-failed')
    ) {
      store.dispatch('user/removeStateA')
      sessionStorage.removeItem('user')
      console.log('Session Expired! Please Login')
      app.router.replace('/')
      return
    }

    if (networkError) {
      console.log('Network error! Please check your connection')
    }
  })

  const surveyHTTPLink = new HttpLink({
    uri: 'http://localhost:8080/v1/graphql',
  })

  const surveyWSLink = new WebSocketLink({
    uri: 'ws://localhost:8080/v1/graphql',
    options: {
      reconnect: true,
      lazy: true,
      connectionParams,
    },
  })

  let authLink = (operation, forward) => {
    const { headers } = operation.getContext()
    if (store.getters['user/isLogged_in']) {
      operation.setContext({
        headers: {
          authorization: `Bearer ${store.getters['user/getToken']}`,
          ...headers,
        },
      })
    }
    return forward(operation)
  }

  const getDefinition = ({ query }) => {
    const definition = getMainDefinition(query)
    return (
      definition.kind === 'OperationDefinition' &&
      definition.operation === 'subscription'
    )
  }

  const surveyLink = split(getDefinition, surveyWSLink, surveyHTTPLink)

  authLink = new ApolloLink(authLink)

  const surveyApolloClient = new ApolloClient({
    link: from([errorLink, authLink, surveyLink]),
    cache: new InMemoryCache({
      addTypename: false,
    }),
    connectToDevTools: true,
  })

  const apolloProvider = new VueApollo({
    defaultClient: surveyApolloClient,
    clients: {
      survey: surveyApolloClient,
    },
  })

  Vue.use(VueApollo)

  Vue.mixin({
    apolloProvider,
  })
}
