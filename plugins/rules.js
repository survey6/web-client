import Vue from 'vue'

export default function () {
  Vue.mixin({
    props: {
      rules: {
        type: Object,
        default: () => ({
          required: [(v) => !!v || 'ይህ ቦታ አስፈላጊ ነው'],
          et_phone_number_nullable: [
            (v) =>
              !v ||
              /^\9\d{8}$/.test(v) ||
              /^\9\d{8}$/.test(v) ||
              'ያልተስተካከለ የኢትዮጵያ የስልክ መዋቅር',
          ],
        }),
      },
    },
  })
}
