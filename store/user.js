export const state = () => ({
  user: undefined,
})

export const getters = {
  getUser: (state) => {
    return state.user
  },
  getToken: (state) => {
    return state.user.token
  },
  isLogged_in: (state) => {
    return state.user !== undefined && state.user !== null
  },
}

export const mutations = {
  setUserData(state, user) {
    sessionStorage.setItem('user', JSON.stringify(user))
    state.user = JSON.parse(JSON.stringify(user))
  },
  removeState(state) {
    state.user = undefined
    sessionStorage.removeItem('user')
  },
}

export const actions = {
  removeStateA({ commit }) {
    commit('removeState')
  },
}
